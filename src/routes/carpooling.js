const Boom = require("@hapi/boom");
const express = require("express");
const CarpoolingService = require("../services/carpooling");

function carPoolingApi(app) {
  const router = express.Router();
  app.use("/", router);

  const carpoolingService = new CarpoolingService();

  /**
   * GET Status
   * Indicates whether the service has started successfully and is ready to accept requests.
   */
  router.get("/status", async function (req, res, next) {
    try {
      let statusCode,
        message = "Not ready";

      const isReady = await carpoolingService.getStatus();

      if (isReady) {
        statusCode = 200;
        message = "OK";
        res.status(statusCode).json({
          statusCode,
          message,
        });
      } else {
        next(Boom.unauthorized("Access denied"));
      }
    } catch (err) {
      next(err);
    }
  });

  /**
   * PUT Status
   * Load the list of available cars in the service and remove all previous data (existing journeys and cars)
   */
  router.put("/cars", async function (req, res, next) {
    if (
      req.body === undefined ||
      req.body === null ||
      !Object.entries(req.body).length ||
      !req.is("application/json")
    ) {
      res.status(400).send("Bad request").end();
      return;
    }

    try {
      const { body: carList } = req;

      const availableCars = await carpoolingService.getCarAvailability({
        carList,
      });
      const statusCode = 200;

      res.status(statusCode).json({
        statusCode,
        data: availableCars,
        message: "Available cars listed",
      });
    } catch (err) {
      next(err);
    }
  });

  /**
   * POST Journey
   * A group of people requests to perform a journey
   */
  router.post("/journey", async function (req, res, next) {
    if (
      req.body === undefined ||
      req.body === null ||
      !Object.entries(req.body).length ||
      !req.is("application/json")
    ) {
      res.status(400).send("Bad request").end();
      return;
    }

    try {
      const { body: journey } = req;

      // Validate that journey.people attribute is a number and is in range between 1 and 6
      if (
        journey.people < 7 &&
        journey.people > 0 &&
        Number.isInteger(journey.people)
      ) {
        const journeyAvailabity = await carpoolingService.getJourneyAvailability(
          {
            journey,
          }
        );
        let statusCode;

        if (journeyAvailabity) {
          const journeyId = await carpoolingService.createJourney({
            journey,
          });
          statusCode = 202;
        } else {
          statusCode = 200;
        }
        res.status(statusCode).end();
      } else {
      }
    } catch (err) {
      next(err);
    }
  });

  /**
   * POST Dropoff
   * A group of people requests to be dropped off. Whether they traveled or not
   */
  router.post("/dropoff", async function (req, res, next) {
    if (
      req.body === undefined ||
      req.body === null ||
      req.body.ID === undefined ||
      req.body.ID === null ||
      !req.is("application/x-www-form-urlencoded")
    ) {
      res.status(400).send("Bad request").end();
      return;
    }

    try {
      const { ID } = req.body;
      let statusCode;

      groupPeople = await carpoolingService.dropoff(ID);

      if (ID && groupPeople) {
        if (groupPeople.isTravelling) {
          statusCode = 200;
          message = "Group dropped off correctly";
        } else {
          statusCode = 204;
        }

        res.status(statusCode).end();
      } else {
        res.status(404).end();
      }
    } catch (err) {
      next(err);
    }
  });

  router.post("/locate", async function (req, res, next) {
    if (
      req.body === undefined ||
      req.body === null ||
      req.body.ID === undefined ||
      req.body.ID === null ||
      !req.is("application/x-www-form-urlencoded")
    ) {
      res.status(400).send("Bad request").end();
      return;
    }

    try {
      const { ID } = req.body;
      let car = {};
      const data = await carpoolingService.locate(ID);
      let statusCode = 200;

      if (Object.entries(data).length) {
        if (data.car) {
          statusCode = 200;
          car = data.car;
        } else {
          statusCode = 204;
        }

        res.status(statusCode).json(car).end();
      } else {
        res.status(404).end();
      }
    } catch (err) {
      next(err);
    }
  });
}

module.exports = carPoolingApi;
