// Import modules
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet");
const morgan = require("morgan");

// Import routes
const carpoolingApi = require("./routes/carpooling");
const notFoundHandler = require("./utils/middleware/notFoundHandler.js");

// Environment Variables
const { config } = require("./config/index");

// Middlewares
// Content Type application/json
app.use(bodyParser.json());
// Content Type application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(helmet());
app.use(morgan("dev"));

// Routes
carpoolingApi(app);

// 404 error
app.use(notFoundHandler);

// Error Middlewares
const {
  logErrors,
  wrapErrors,
  errorHandler,
} = require("./utils/middleware/errorsHandler.js");
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

app.listen(config.port, function () {
  console.log(`Listening http://localhost:${config.port}`);
});
