const { carsMock, peopleMock, journeysMock } = require("../utils/mocks/mock");

class CarPoolingService {
  async getStatus() {
    return true;
  }

  async getCar(carId) {
    const car = carsMock.find((car) => car.id == carId);
    return car || "";
  }

  async getCarAvailability({ carList }) {
    const availableCars = await Promise.resolve(carsMock);
    return availableCars || [];
  }

  async getJourneyAvailability({ journey }) {
    const carAvailabity = await this.getCarAvailability({});

    const journeyAvailabity = carAvailabity.find(
      (car) => car.seats == journey.people
    );
    return journeyAvailabity;
  }

  async createJourney({ journey }) {
    let journeyId;
    const journeyExists = journeysMock.some(
      (journeyMock) => journeyMock.id == journey.id
    );
    // verify if journey was alredy registered
    if (!journeyExists) {
      journeysMock.push(journey);
    }
    journeyId = journey.id;
    return journeyId || [];
  }

  async dropoff(groupId) {
    const data = await this.locate(groupId);
    return data.groupPeople || "";
  }

  async locate(groupId) {
    const groupPeople = peopleMock.find((people) => people.id == groupId);
    let data = {};
    let car;
    if (groupPeople) {
      data.groupPeople = groupPeople;
      if (groupPeople.isTravelling) {
        car = await this.getCar(groupPeople.carId);
        data.car = car;
      } else {
        data.car = "";
      }
    }

    return data || {};
  }
}

module.exports = CarPoolingService;
