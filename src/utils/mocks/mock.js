const carsMock = [
  {
    id: 42,
    seats: 4,
  },
  {
    id: 2,
    seats: 6,
  },
  {
    id: 3,
    seats: 1,
  },
  {
    id: 4,
    seats: 2,
  },
  {
    id: 5,
    seats: 1,
  },
  {
    id: 6,
    seats: 2,
  },
  {
    id: 7,
    seats: 4,
  },
  {
    id: 8,
    seats: 6,
  },
  {
    id: 9,
    seats: 2,
  },
  {
    id: 10,
    seats: 6,
  },
  {
    id: 11,
    seats: 6,
  },
  {
    id: 12,
    seats: 6,
  },
];

const peopleMock = [
  {
    id: 42,
    people: 3,
    isTravelling: true,
    journeyId: 1,
    carId: 42,
  },
  {
    id: 2,
    people: 6,
    isTravelling: true,
    journeyId: 2,
    carId: 2,
  },
  {
    id: 3,
    people: 2,
    isTravelling: true,
    journeyId: 3,
    carId: 3,
  },
  {
    id: 4,
    people: 4,
    isTravelling: true,
    journeyId: 4,
    carId: 4,
  },
  {
    id: 5,
    people: 5,
    isTravelling: false,
    journeyId: 5,
    carId: 5,
  },
  {
    id: 6,
    people: 3,
    isTravelling: false,
    journeyId: 6,
    carId: 6,
  },
];

const journeysMock = [
  {
    id: 123,
    carId: 21,
    driverName: "Jon Doe",
    userId: 12,
    userName: "Lio Messi",
    carPooling: true,
    groupSize: 5,
  },
];

class CarpoolingServiceMock {
  async getStatus() {
    return true;
  }

  async getCar(carId) {
    const car = carsMock.find((car) => car.id == carId);
    return car || "";
  }

  async getCarAvailability({ carList }) {
    const availableCars = await Promise.resolve(carsMock);
    return availableCars || [];
  }

  async getJourneyAvailability({ journey }) {
    const carAvailabity = await this.getCarAvailability({});

    const journeyAvailabity = carAvailabity.find(
      (car) => car.seats == journey.people
    );
    return journeyAvailabity;
  }

  async createJourney({ journey }) {
    let journeyId;
    const journeyExists = journeysMock.some(
      (journeyMock) => journeyMock.id == journey.id
    );
    // verify if journey was alredy registered
    if (!journeyExists) {
      journeysMock.push(journey);
    }
    journeyId = journey.id;
    return journeyId || [];
  }

  async dropoff(groupId) {
    const data = await this.locate(groupId);
    return data.groupPeople || "";
  }

  async locate(groupId) {
    const groupPeople = peopleMock.find((people) => people.id == groupId);
    let data = {};
    let car;
    if (groupPeople) {
      data.groupPeople = groupPeople;
      if (groupPeople.isTravelling) {
        car = await this.getCar(groupPeople.carId);
        data.car = car;
      } else {
        data.car = "";
      }
    }

    return data || "";
  }
}

module.exports = {
  carsMock,
  peopleMock,
  journeysMock,
  CarpoolingServiceMock,
};
