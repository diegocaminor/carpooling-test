const assert = require("assert");
const proxyquire = require("proxyquire"); // proxyquire will replace CarpoolingService with CarpoolingServiceMock

const {
  carsMock,
  peopleMock,
  jouneyMock,
  CarpoolingServiceMock,
} = require("../utils/mocks/mock");
const testServer = require("../utils/testServer");

describe("routes - carpooling", function () {
  const route = proxyquire("../routes/carpooling", {
    "../services/carpooling": CarpoolingServiceMock,
  });

  const request = testServer(route);
  describe("GET /status", function () {
    this.slow(1000);
    it("should respond with status 200", function (done) {
      request.get("/status").expect(200, done);
    });
  });

  describe("PUT /cars", function () {
    it("should respond with status 400", function (done) {
      request.put("/cars").expect(400, done);
    });

    it("should respond with status 200 when the list is registered correctly", function (done) {
      request
        .put("/cars")
        .send({
          id: 1,
        })
        .expect(200, done);
    });

    it("should respond with the list of available cars", function (done) {
      request
        .put("/cars")
        .send({
          id: 1,
        })
        .set("Accept", "application/json")
        .end((err, res) => {
          assert.deepEqual(res.body, {
            statusCode: 200,
            data: carsMock,
            message: "Available cars listed",
          });

          done();
        });
    });
  });

  describe("POST /journey", function () {
    it("should respond with status 200 when a group of people requested to perform a journey but no registered", function (done) {
      request
        .post("/journey")
        .send({
          id: 1,
          people: 5,
        })
        .expect(200, done);
    });

    it("should respond with status 202 when the group is registered correctly", function (done) {
      request
        .post("/journey")
        .send({
          id: 1,
          people: 4,
        })
        .expect(202, done);
    });
  });

  describe("POST /dropoff", function () {
    it("should respond with status 200 when the group is unregistered correctly", function (done) {
      request
        .post("/dropoff")
        .send("ID=4") // x-www-form-urlencoded upload
        .expect(200, done);
    });

    it("should respond with status 204", function (done) {
      request.post("/dropoff").send("ID=6").expect(204, done);
    });
  });

  describe("POST /locate", function () {
    it("should respond with status 200 when the group is assigned to a car", function (done) {
      request.post("/locate").send("ID=4").expect(200, done);
    });

    it("should respond with status 204 when the group is waiting to be assigned to a car", function (done) {
      request.post("/locate").send("ID=6").expect(204, done);
    });

    it("should respond with the car as the payload when the group is assigned to a car", function (done) {
      request
        .post("/locate")
        .send("ID=4")
        .end((err, res) => {
          assert.deepEqual(res.body, {
            statusCode: 200,
            data: carsMock[3],
            message: "Group registered correctly",
          });

          done();
        });
    });
  });
});
