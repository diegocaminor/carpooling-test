FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json and package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Install dependencies, and not devDependencies
RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 9091
CMD [ "npm", "start" ]